<?php
/**
 * @file
 * sarnia_example.features.inc
 */

/**
 * Implements hook_views_api().
 */
function sarnia_example_views_api() {
  return array("version" => "3.0");
}

/**
 * Implements hook_default_search_api_index().
 */
function sarnia_example_default_search_api_index() {
  $items = array();
  $items['sarnia_solr_server_drs'] = entity_import('search_api_index', '{
    "name" : "Solr Server - DRS (Sarnia index)",
    "machine_name" : "sarnia_solr_server_drs",
    "description" : "This index is managed by the Sarnia module.",
    "server" : "solr_server_drs",
    "item_type" : "sarnia_solr_server_drs",
    "options" : {
      "cron_limit" : 0,
      "fields" : {
        "abstract" : {
          "name" : "abstract",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "all" : { "name" : "all", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "colid" : { "name" : "colid", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "create" : { "name" : "create", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "creation_year" : {
          "name" : "creation_year",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "dateBegin" : {
          "name" : "dateBegin",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "dateEnd" : { "name" : "dateEnd", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "delete" : { "name" : "delete", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "deposit_date" : {
          "name" : "deposit_date",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "deposit_year" : {
          "name" : "deposit_year",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "embargodate" : {
          "name" : "embargodate",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "genre" : { "name" : "genre", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "genre_string" : {
          "name" : "genre_string",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "identifier" : {
          "name" : "identifier",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "intpid" : { "name" : "intpid", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "iristitle" : {
          "name" : "iristitle",
          "indexed" : false,
          "type" : "none",
          "boost" : "1.0"
        },
        "iristype" : {
          "name" : "iristype",
          "indexed" : false,
          "type" : "none",
          "boost" : "1.0"
        },
        "irtitle" : { "name" : "irtitle", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "irtype" : { "name" : "irtype", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "last_modified_date" : {
          "name" : "last_modified_date",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "name" : { "name" : "name", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "name_all" : {
          "name" : "name_all",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "name_string" : {
          "name" : "name_string",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "namespace" : {
          "name" : "namespace",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "note" : { "name" : "note", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "note_all" : {
          "name" : "note_all",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "origin_all" : {
          "name" : "origin_all",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "ownerid" : { "name" : "ownerid", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "pid" : { "name" : "pid", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "pubPlace" : {
          "name" : "pubPlace",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "publisher" : {
          "name" : "publisher",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "read" : { "name" : "read", "indexed" : true, "type" : "none", "boost" : "1.0" },
        "role" : { "name" : "role", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "subTitle" : {
          "name" : "subTitle",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "subject" : { "name" : "subject", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "subject_all" : {
          "name" : "subject_all",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "subject_string" : {
          "name" : "subject_string",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "thumbnail" : {
          "name" : "thumbnail",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "title" : { "name" : "title", "indexed" : true, "type" : "text", "boost" : "1.0" },
        "title_all" : {
          "name" : "title_all",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "title_display" : {
          "name" : "title_display",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "title_string" : {
          "name" : "title_string",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "type_str" : {
          "name" : "type_str",
          "indexed" : true,
          "type" : "none",
          "boost" : "1.0"
        },
        "type_txt" : {
          "name" : "type_txt",
          "indexed" : true,
          "type" : "text",
          "boost" : "1.0"
        },
        "update" : { "name" : "update", "indexed" : true, "type" : "none", "boost" : "1.0" }
      }
    },
    "enabled" : "1",
    "read_only" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}

/**
 * Implements hook_default_search_api_server().
 */
function sarnia_example_default_search_api_server() {
  $items = array();
  $items['solr_server_drs'] = entity_import('search_api_server', '{
    "name" : "Solr Server - DRS",
    "machine_name" : "solr_server_drs",
    "description" : "",
    "class" : "sarnia_solr_service",
    "options" : {
      "host" : "libtomcat.neu.edu",
      "port" : "8080",
      "path" : "\\/solr\\/fedora",
      "http_user" : "",
      "http_pass" : "",
      "excerpt" : 0,
      "retrieve_data" : 0,
      "highlight_data" : 0,
      "http_method" : "POST"
    },
    "enabled" : "1",
    "rdf_mapping" : []
  }');
  return $items;
}
