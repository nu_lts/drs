<?php
/**
 * @file
 * sarnia_example.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function sarnia_example_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'drs_solr';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'search_api_index_sarnia_solr_server_drs';
  $view->human_name = 'DRS Solr';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'DRS Solr - Search';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'none';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '10';
  $handler->display->display_options['style_plugin'] = 'list';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Field: Solr Server - DRS (Sarnia index): Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Field: Solr Server - DRS (Sarnia index): title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['link_to_entity'] = 1;
  /* Field: Solr Server - DRS (Sarnia index): abstract */
  $handler->display->display_options['fields']['abstract']['id'] = 'abstract';
  $handler->display->display_options['fields']['abstract']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['abstract']['field'] = 'abstract';
  $handler->display->display_options['fields']['abstract']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): all */
  $handler->display->display_options['fields']['all']['id'] = 'all';
  $handler->display->display_options['fields']['all']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['all']['field'] = 'all';
  $handler->display->display_options['fields']['all']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): Data: abstract */
  $handler->display->display_options['fields']['solr_document']['id'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['solr_document']['field'] = 'solr_document';
  $handler->display->display_options['fields']['solr_document']['label'] = 'abstract';
  $handler->display->display_options['fields']['solr_document']['solr_property'] = 'abstract';
  $handler->display->display_options['fields']['solr_document']['formatter'] = '0';
  $handler->display->display_options['fields']['solr_document']['is_multivalue'] = TRUE;
  /* Field: Solr Server - DRS (Sarnia index): Debug output */
  $handler->display->display_options['fields']['rendered_entity']['id'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['table'] = 'views_entity_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['rendered_entity']['field'] = 'rendered_entity';
  $handler->display->display_options['fields']['rendered_entity']['link_to_entity'] = 1;
  /* Field: Solr Server - DRS (Sarnia index): genre */
  $handler->display->display_options['fields']['genre']['id'] = 'genre';
  $handler->display->display_options['fields']['genre']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['genre']['field'] = 'genre';
  $handler->display->display_options['fields']['genre']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): Id */
  $handler->display->display_options['fields']['id_1']['id'] = 'id_1';
  $handler->display->display_options['fields']['id_1']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['id_1']['field'] = 'id';
  $handler->display->display_options['fields']['id_1']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): identifier */
  $handler->display->display_options['fields']['identifier']['id'] = 'identifier';
  $handler->display->display_options['fields']['identifier']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['identifier']['field'] = 'identifier';
  $handler->display->display_options['fields']['identifier']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): Item language */
  $handler->display->display_options['fields']['search_api_language']['id'] = 'search_api_language';
  $handler->display->display_options['fields']['search_api_language']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['search_api_language']['field'] = 'search_api_language';
  $handler->display->display_options['fields']['search_api_language']['link_to_entity'] = 0;
  $handler->display->display_options['fields']['search_api_language']['format_name'] = 1;
  /* Field: Solr Server - DRS (Sarnia index): name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): name_all */
  $handler->display->display_options['fields']['name_all']['id'] = 'name_all';
  $handler->display->display_options['fields']['name_all']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['name_all']['field'] = 'name_all';
  $handler->display->display_options['fields']['name_all']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): note */
  $handler->display->display_options['fields']['note']['id'] = 'note';
  $handler->display->display_options['fields']['note']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['note']['field'] = 'note';
  $handler->display->display_options['fields']['note']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): note_all */
  $handler->display->display_options['fields']['note_all']['id'] = 'note_all';
  $handler->display->display_options['fields']['note_all']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['note_all']['field'] = 'note_all';
  $handler->display->display_options['fields']['note_all']['link_to_entity'] = 0;
  /* Field: Solr Server - DRS (Sarnia index): type_txt */
  $handler->display->display_options['fields']['type_txt']['id'] = 'type_txt';
  $handler->display->display_options['fields']['type_txt']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['fields']['type_txt']['field'] = 'type_txt';
  $handler->display->display_options['fields']['type_txt']['link_to_entity'] = 0;
  /* Filter criterion: Search: Fulltext search */
  $handler->display->display_options['filters']['search_api_views_fulltext']['id'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['table'] = 'search_api_index_sarnia_solr_server_drs';
  $handler->display->display_options['filters']['search_api_views_fulltext']['field'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['exposed'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator_id'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['label'] = 'Fulltext search';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['use_operator'] = TRUE;
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['operator'] = 'search_api_views_fulltext_op';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['identifier'] = 'search_api_views_fulltext';
  $handler->display->display_options['filters']['search_api_views_fulltext']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['filters']['search_api_views_fulltext']['fields'] = array(
    'all' => 'all',
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['hide_admin_links'] = FALSE;
  $handler->display->display_options['path'] = 'solr/search';
  $handler->display->display_options['menu']['type'] = 'normal';
  $handler->display->display_options['menu']['title'] = 'DRS Solr View';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['context'] = 0;
  $export['drs_solr'] = $view;

  return $export;
}
