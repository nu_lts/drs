<?php
/**
*
*  This include function is for php Functions you want to add to the process of parsing out that XML!!!!!
*
*
*
*
*
*
*
*
*/
//Register the NameSpaces and return an array with name and url of namespaces schema.




function registerNameSpaces($simpleXMLObject){
  $nameSpacesArray = $simpleXMLObject -> getNamespaces(TRUE);
  foreach ($nameSpacesArray as $namespace => $namespaceUrl){
    $simpleXMLObject -> registerXPathNamespace($namespace, $namespaceUrl);
  }
  return $simpleXMLObject;
}


function convertPidToId($pid){
    return intval(str_replace('neu:', '', $pid));
}







///I want a function that takes simple XML object and namespace and returns a associative array of the elements name and elements string value.

function metaDataArray($simpleXMLObject, $namespace){
  
  //$return = $simpleXMLObject -> xpath('//metadata')[0] -> children($namespace) -> count();
  return $return;
}


///Archived functions
function xml2phpArray($xml, $arr) {
    $iter = 0;
    $namespaces = array_merge(array('' => ''), $xml->getDocNamespaces(true));
    foreach ($namespaces as $namespace => $namespaceUrl) {
        foreach ($xml->children($namespaceUrl) as $b) {
            $a = $b->getName();

            if ($b->children($namespaceUrl)) {
                $arr[$a][$iter] = array();
                $arr[$a][$iter] = xml2phpArray($b, $namespaces, $arr[$a][$iter]);
            }
            else {
                $arr[$a] = trim($b[0]);
            }

            $iter++;
        }
    }

    return $arr;
}


function xml2array ( $xmlObject, $out = array () )
{
  foreach ( (array) $xmlObject as $index => $node )
  {
    $out[$index] = ( is_object ( $node ) ) ? xml2array ( $node ) : $node;
  }
  return $out;
}

function buildCSubNav($object){
        $navHtml = '';
        $linkArray = array();
        foreach ($object as $sub){
             $linkArray[] = '<a href="/drupal/drs/view/' . $sub -> pid . '">' . $sub -> label .'</a>';             
        }
        foreach($linkArray as $link){
            $navHtml .= '<li>' . $link . '</li>';
        }
        $navHtml = '<nav><ul>' . $navHtml .'</ul></nav>';
        return $navHtml;
}




function xmlToArray($xml, $options = array()) {
    $defaults = array(
        'namespaceSeparator' => ':',//you may want this to be something other than a colon
        'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
        'alwaysArray' => array(),   //array of xml tag names which should always become arrays
        'autoArray' => true,        //only create arrays for tags which appear more than once
        'textContent' => '$',       //key used for the text content of elements
        'autoText' => true,         //skip textContent key if node has no attributes or child nodes
        'keySearch' => false,       //optional search and replace on tag and attribute names
        'keyReplace' => false       //replace values for above search values (as passed to str_replace())
    );
    $options = array_merge($defaults, $options);
    $namespaces = $xml->getDocNamespaces();
    $namespaces[''] = null; //add base (empty) namespace
 
    //get attributes from all namespaces
    $attributesArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->attributes($namespace) as $attributeName => $attribute) {
            //replace characters in attribute name
            if ($options['keySearch']) $attributeName =
                    str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
            $attributeKey = $options['attributePrefix']
                    . ($prefix ? $prefix . $options['namespaceSeparator'] : '')
                    . $attributeName;
            $attributesArray[$attributeKey] = (string)$attribute;
        }
    }
 
    //get child nodes from all namespaces
    $tagsArray = array();
    foreach ($namespaces as $prefix => $namespace) {
        foreach ($xml->children($namespace) as $childXml) {
            //recurse into child nodes
            $childArray = xmlToArray($childXml, $options);
            list($childTagName, $childProperties) = each($childArray);
 
            //replace characters in tag name
            if ($options['keySearch']) $childTagName =
                    str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
            //add namespace prefix, if any
            if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;
 
            if (!isset($tagsArray[$childTagName])) {
                //only entry with this key
                //test if tags of this type should always be arrays, no matter the element count
                $tagsArray[$childTagName] =
                        in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
                        ? array($childProperties) : $childProperties;
            } elseif (
                is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
                === range(0, count($tagsArray[$childTagName]) - 1)
            ) {
                //key already exists and is integer indexed array
                $tagsArray[$childTagName][] = $childProperties;
            } else {
                //key exists so convert to integer indexed array with previous value in position 0
                $tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
            }
        }
    }
 
    //get text content of node
    $textContentArray = array();
    $plainText = trim((string)$xml);
    if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;
 
    //stick it all together
    $propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
            ? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;
 
    //return node as array
    return array(
        $xml->getName() => $propertiesArray
    );
}

class Lister {

  private $list = array();
  private $enclose = array('begin'=>'', 'end'=>'');
  private $delimit = '';

  public function ordered ($array, $enclose='') {
    $list = '';
    if (!empty($array) && is_array($array)) {
      $this->set_vars ($array, $enclose);
      $list .= $this->create ('ol');
      $this->reset_vars();
    }
    return $list;
  }

  public function unordered ($array, $enclose='') {
    $list = '';
    if (!empty($array) && is_array($array)) {
      $this->set_vars ($array, $enclose);
      $list .= $this->create ('ul');
      $this->reset_vars();
    }
    return $list;
  }

  public function definition ($array) {
    $list = '';
    if (!empty($array) && is_array($array)) {
      $list .= $this->indent(1) . '<dl>';
      foreach ($array as $tag => $define) {
        $list .=  $this->indent(2) . '<dt>' . $tag . '</dt>';
        $list .=  $this->indent(3) . '<dd>' . $define . '</dd>';
      }
      $list .= $this->indent(1) . '</dl>';
    }
    return $list;
  }

  public function custom ($delimit, $array, $begin='', $end='', $enclose='') {
    $list = '';
    if (!empty($array) && is_array($array)) {
      $this->set_vars ($array, $enclose, $delimit, $begin, $end);
      $list .= $this->delimiter();
      $this->reset_vars();
    }
    return $list;
  }
  
  public function make_multi_dimensional ($array) {
    if (isset($array[0]) && is_array($array[0])) return $array; // already multi-dimensional
    $new = array();
    $multi_dimensional = array();
    foreach ($array as $key => $value) {
       list($depth, $value) = $this->array_depth($value);
       $new[$key+1][$depth] = $value;
    }
    $parent[1] = 0; // $depth => $parent
    foreach ($new as $key => $value) {
      list($depth, $value) = each($value);
      $group = $parent[$depth];
      $multi_dimensional[$group][$key] = $value;
      if (isset($new[$key+1])) { // let's see what we have next
        $next = each($new[$key+1]);
        $next_depth = $next['key'];
        if ($next_depth > $depth) { // then this may be a new parent
          if (!isset($parent[$next_depth])) {
            $parent[$next_depth] = $key;
          }
        } elseif ($next_depth < $depth) {
          $difference = $depth - $next_depth;
          for ($i=0; $i<$difference; $i++) {
            array_pop($parent); // no longer related
          }
        }
      } // end if $next
    }
    return $multi_dimensional;
  }

  private function set_vars ($array, $enclose='', $delimit='', $begin='', $end='') {
    $this->list = $this->make_multi_dimensional ($array);
    if (!empty($begin)) $this->enclose['begin'] .= $begin;
    if (!empty($enclose) && is_array($enclose)) {
      $this->enclose['begin'] .= '<' . implode('><' , $enclose) . '>';
      krsort($enclose);
      $this->enclose['end'] .= '</' . implode('></' , $enclose) . '>';
    }
    if (!empty($delimit)) $this->delimit = $delimit;
    if (!empty($end))  $this->enclose['end'] .= $end;
  }

  private function reset_vars () {
    $this->list = array();
    $this->enclose = array('begin'=>'', 'end'=>'');
    $this->delimit = '';
  }
  
  private function array_depth ($array) {
    $depth = 1;
    if (is_array($array)) {
      while (is_array($array)) {
        list($key, $array) = each($array);
        $depth++;
      }
    }
    $value = $array;
    return array($depth, $value);
  }

  private function create ($tag, $parent='') {
    $list = '';
    if (empty($parent)) $parent = $this->list[0];
    if (!empty($parent)) {
      static $num = 0;
      $num++;
      $list .= $this->indent($num) . '<' . $tag . '>';
      foreach ($parent as $child => $show) {
        if (!empty($show) || isset($this->list[$child])) {
          $list .= $this->indent($num) . '<li>' . $this->enclose['begin'] . $show . $this->enclose['end'];
          if (isset($this->list[$child])) $list .= $this->create($tag, $this->list[$child]);
          $list .= '</li>';
        }
      }
      $list .= $this->indent($num) . '</' . $tag . '>';
      $num--;
    }
    return $list;
  }

  private function delimiter ($delimiter='', $parent='') {
    $list = '';
    if (empty($parent)) $parent = $this->list[0];
    $delimiter .= $this->delimit;
    if (!empty($parent)) {
      static $num = 0;
      $num++;
      foreach ($parent as $child => $show) {
        if (!empty($show) || isset($this->list[$child])) {
          $list .= $this->indent($num) . $delimiter . $this->enclose['begin'] . $show . $this->enclose['end'];
          if (isset($this->list[$child])) $list .= $this->delimiter ($delimiter, $this->list[$child]);
        }
      }
      $num--;
    }
    return $list;
  }

  private function indent ($num) {
    $indent = "\n";
    for ($i=0; $i<$num; $i++) {
      $indent .= '  ';
    }
    return $indent;
  }

}